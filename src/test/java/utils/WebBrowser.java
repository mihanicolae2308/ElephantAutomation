package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Class that defines a web browser object, providing a method for getting the actual driver depending on the selected browser.
 * @author Mihai Nicolae, 10-Oct-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class WebBrowser {

    public enum Browsers {
        FIREFOX,
        CHROME,
        IE,
        HTMLUNIT
    }

    /**
     * Method for getting the web browser driver.
     * @param   browser the browser for which the driver needs to be retrieved.
     * @return  the web browser driver fro the selected browser type.
     * @throws  Exception
     */
    public static WebDriver getDriver(Browsers browser) throws Exception {
        WebDriver driver = null;

        switch (browser) {
            case IE: {
                System.setProperty("webdriver.ie.driver", "src\\main\\resources\\drivers\\IEDriverServer32.exe");
                return new InternetExplorerDriver();
            }
            case CHROME: {
                System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");

                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                return new ChromeDriver(options);
            }
            case FIREFOX: {
                System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
                ProfilesIni profile = new ProfilesIni();
                FirefoxProfile myprofile = profile.getProfile("default");

                DesiredCapabilities dc = new DesiredCapabilities();
                dc.setCapability(FirefoxDriver.PROFILE, myprofile);
                dc.setCapability("browserName", "safari");
                return new FirefoxDriver(dc);
            }
            case HTMLUNIT: {
                return new HtmlUnitDriver();
            }
            default: {
                throw new Exception("Invalid browser");
            }
        }
    }
}
