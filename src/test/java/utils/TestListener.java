package utils;

import org.testng.ITestResult;

/**
 * Created by Mihai on 09-Nov-17.
 */
public class TestListener {
    public void onTestStart(ITestResult it) {
        String testMethod = it.getMethod().getMethodName();
        String testDescr = it.getMethod().getDescription();

        System.out.println("TEST : " + testMethod);
        if (testDescr!=null && testDescr.length() > 0) {
            System.out.println("DESCRIPTION : " + testDescr);
        }
    }

    public void onTestSuccess(ITestResult it) {
        System.out.println("[PASSED] TEST : " + it.getMethod().getMethodName() + " is " + it.getStatus());
    }

    public void onTestFailure(ITestResult it){
        System.out.println("[FAILED] TEST : " + it.getMethod().getMethodName() + " is " + it.getStatus());
    }

    public void onTestSkipped(ITestResult it){
        System.out.println("[SKIPPED] TEST : " + it.getMethod().getMethodName() + " is " + it.getStatus());
    }
}
