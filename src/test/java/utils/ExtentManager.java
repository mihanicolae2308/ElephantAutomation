package utils;

import com.relevantcodes.extentreports.ExtentReports;

/**
 * Created by Mihai on 25-Nov-17.
 */

public class ExtentManager {
    private static ExtentReports extentReport;

    public synchronized static ExtentReports getReporter(String filePath) {
        if (extentReport == null) {
            extentReport = new ExtentReports(filePath, true);

            extentReport
                    .addSystemInfo("Host Name", "ASUS Laptop")
                    .addSystemInfo("Environment", "QA");
        }

        return extentReport;
    }
}
