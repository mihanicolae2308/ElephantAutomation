package utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Base class for tests.
 * It contains fields and methods to be used in the test classes derived from this class.
 * @author Mihai Nicolae, 10-Oct-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class BaseTest {
    public String username;
    public String password;
    public String database;
    public String homePage;

    public WebDriver driver = null;

    protected static ExtentReports extentReport;
    protected static ExtentTest test;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    protected String reportFile = "D:\\Reports\\Report_" + sdf.format(timestamp) + ".html";
    protected String screenshotDir = "D:\\Screenshots\\Screenshots_" + sdf.format(timestamp);

    WebBrowser.Browsers browserType;

    @BeforeTest
    /**
     * Method for starting a web browser driver.
     * It is intended to be run before each test.
     */
    public void startDriver() {
        try {
            driver = WebBrowser.getDriver(WebBrowser.Browsers.CHROME);
        } catch (Exception ex) {
            System.out.println("Browser specified is not on the supported browser list! Please use firefox, chrome, ie or htmlunit as argument!");
        }
    }
    @Parameters({"username", "password", "database", "homePage"})
    @BeforeTest
    /**
     * Method for getting database login credentials and home page from the configuration file,
     * so that they are available for the data providers using data from the test data database.
     * @param u database login username.
     * @param p database login password.
     * @param d database to be connected.
     */
    public void getParameters(String u, String p, String d, String h) {
        username=u;
        password=p;
        database=d;
        homePage=h;
    }

    @AfterTest
    /**
     * This method is intended to run after each test in order to close the web browser driver.
     */
    public void closeDriver() {
        if (driver != null) {
            driver.close();
        } else {
            System.out.println("No driver to be closed.");
        }
    }

    /**
     * Method used for the situations when test data retrieved from the database for a specific
     * field is null, but the methods that need this data as parameters do not accept null.
     * In this case null is replaced by an empty string ("").
     * @param   str the string that needs to be converted
     * @return  an empty string if the parameter is null or the original string if it is not null.
     */
    public String fixNullString(String str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    }

    public void mySleeper(int millis) {
        try {
            Thread.sleep(millis);
        }
        catch (Exception ex) {

        }
    }

    public void mySleeper() {
        mySleeper(500);
    }

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        extentReport = ExtentManager.getReporter(reportFile);
        File dir = new File(screenshotDir);
        dir.mkdir();
    }

//    @AfterSuite
//    protected void afterSuite() {
//        extentReport.close();
//    }

    @AfterMethod
    protected void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            test.log(LogStatus.FAIL, result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP) {
            test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } else {
            test.log(LogStatus.PASS, "Test passed");
        }

        extentReport.endTest(test);
        extentReport.flush();
    }
}