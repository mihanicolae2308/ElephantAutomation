package elephantTests;

import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.PublishersPage;
import pages.SpecialOffersPage;
import utils.BaseTest;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Created by Mihai on 09-Dec-17.
 */
public class SpecialOffersPageTests extends BaseTest {
    @Test
    public void specialOffersPageTest() {
        test = extentReport.startTest("Special Offers Page Test");
        driver.navigate().to(homePage + "oferte-speciale");

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(screenshotDir + "\\" + this.getClass().getName() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        SpecialOffersPage ab = PageFactory.initElements(driver, SpecialOffersPage.class);

        Object obj = ab;
        System.out.println(ab.getClass().getFields().length);
        for (Field field : ab.getClass().getFields()) {
            if (field.getType() == WebElement.class) {
                try {
                    WebElement we = (WebElement) field.get(obj);
                    System.out.println(we.getText());
                    ab.checkElementDisplayed(we);
                    mySleeper(1000);
                    test.log(LogStatus.PASS, "Successfully verified element: " + we.getText());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
