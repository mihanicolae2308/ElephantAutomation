package elephantTests;

import com.relevantcodes.extentreports.LogStatus;
import models.ProductCategoryModel;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.WatchesCategory;
import utils.BaseTest;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Mihai on 13-Dec-17.
 */
public class WatchesCategoryTests extends BaseTest {
    @DataProvider(name = "CSVwatchesCategoryDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a generic model for the tested page.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("watchesCategoryTestData.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            ProductCategoryModel model = new ProductCategoryModel();
            model.setWebElementName(csvData.get(i)[0]);
            model.setExpectedUrl(csvData.get(i)[1]);
            dp[i][0] = model;
        }
        return dp;
    }

    @Test(dataProvider = "CSVwatchesCategoryDataProvider")
    /**
     * Method for running tests for watches category using test data from a CSV data provider
     * @param   pcm a generic model that is used for this page
     * @see     ProductCategoryModel
     */
    public void testWatches(ProductCategoryModel pcm) {
        test = extentReport.startTest("Watches Category Tests");

        driver.navigate().to(homePage + "homepage");
        WatchesCategory pf = PageFactory.initElements(driver, WatchesCategory.class);

        pf.goToCategory(driver.findElement(By.xpath("//A[@data-toggle='dropdown'][text()='Ceasuri']")));

        Object obj = pf;

        for (Field field : pf.getClass().getFields()) {
            System.out.println(field.getName());
            System.out.println(field.getType());
            System.out.println(WebElement.class);
            if (field.getName().equals(pcm.getWebElementName()) && (field.getType() == WebElement.class)) {
                try {
                    WebElement el = (WebElement) field.get(obj);
                    pf.goToSubcategory(el);

                    test.log(LogStatus.PASS, "Testing category " + field.getName());

                    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
                    try {
                        FileUtils.copyFile(scrFile, new File(screenshotDir + "\\" + this.getClass().getName() + ".png"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String currentURL = driver.getCurrentUrl();
                    Assert.assertEquals(currentURL, pcm.getExpectedUrl());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
