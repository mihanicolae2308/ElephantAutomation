package elephantTests;


import com.relevantcodes.extentreports.LogStatus;
import models.RegistrationModel;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;
import utils.BaseTest;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Mihai on 10-Dec-17.
 */
public class RegisterTests extends BaseTest {

    @DataProvider(name = "CSVRegistrationDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a model for the tested page.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("registrationTestData.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            RegistrationModel model = new RegistrationModel();
            model.setFirstName(csvData.get(i)[0]);
            model.setLastName(csvData.get(i)[1]);
            model.setEmail(csvData.get(i)[2]);
            model.setPassword(csvData.get(i)[3]);
            model.setPasswordConfirmation(csvData.get(i)[4]);
            model.setGender(csvData.get(i)[5]);
            model.setConditionsAccepted(csvData.get(i)[6]);
            model.setExpectedSexError(csvData.get(i)[7]);
            model.setExpectedFirstNameError(csvData.get(i)[8]);
            model.setExpectedLastNameError(csvData.get(i)[9]);
            model.setExpectedEmailError(csvData.get(i)[10]);
            model.setExpectedPasswordError(csvData.get(i)[11]);
            model.setExpectedPasswordConfirmationError(csvData.get(i)[12]);
            model.setExpectedTermsAcceptanceError(csvData.get(i)[13]);

            dp[i][0] = model;
        }
        return dp;
    }

    @Test(dataProvider = "CSVRegistrationDataProvider")
    /**
     * Method for running registration negative tests using test data from a CSV data provider.
     * The test scenario verifies that the correct error messages are displayed in case of
     * empty or incorrect values entered in registration fields.
     * @param   rm  a model for registration page
     * @see     RegistrationModel
     */
    public void registrationTestCSV(RegistrationModel rm) {
        test = extentReport.startTest("Register Negative Test");

        driver.navigate().to(homePage + "creeaza-cont");

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(screenshotDir + "\\" + this.getClass().getName() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        RegisterPage rp = PageFactory.initElements(driver, RegisterPage.class);

        rp.register(rm.getFirstName(), rm.getLastName(), rm.getEmail(), rm.getPassword(), rm.getPasswordConfirmation(), rm.getGender(), rm.getConditionsAccepted());

        File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile2, new File(screenshotDir + "\\" + this.getClass().getName() + "_error.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        test.log(LogStatus.PASS, "Testing with " + rm.getFirstName() + " first name, " + rm.getLastName() + " last name, " + rm.getEmail() + " email, " + rm.getPassword() + " password, " +
        rm.getPasswordConfirmation() + " password confirmation, gender=" + rm.getGender() + ", terms accepted=" + rm.getConditionsAccepted());

        rp.checkPageErrors(rm.getFirstName(), rm.getLastName(), rm.getEmail(), rm.getPassword(), rm.getPasswordConfirmation(),rm.getGender(), rm.getConditionsAccepted(),
                rm.getExpectedSexError(), rm.getExpectedFirstNameError(), rm.getExpectedLastNameError(), rm.getExpectedEmailError(), rm.getExpectedPasswordError(),
                rm.getExpectedPasswordConfirmationError(), rm.getExpectedTermsAcceptanceError());

    }


}
