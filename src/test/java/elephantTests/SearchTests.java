package elephantTests;

import com.relevantcodes.extentreports.LogStatus;
import models.RegistrationModel;
import models.SearchModel;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import utils.BaseTest;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class SearchTests extends BaseTest {
    @DataProvider(name = "CSVSearchDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a model for the tested page.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("searchTestData.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            SearchModel model = new SearchModel();
            model.setSearchText(csvData.get(i)[0]);
            model.setElementXpath(csvData.get(i)[1]);

            dp[i][0] = model;
        }
        return dp;
    }

    @Test(dataProvider = "CSVSearchDataProvider")
    /**
     * Method for running search tests using test data from a CSV data provider.
     * @param   sm  a model for search action
     * @see     SearchModel
     */
    public void searchTests(SearchModel sm) {
        test = extentReport.startTest("Search test");

        driver.navigate().to(homePage + "homepage");
        HomePage hp = PageFactory.initElements(driver, HomePage.class);

        hp.search(sm.getSearchText());

        test.log(LogStatus.PASS, "Searching '" + sm.getSearchText() + "'");

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(screenshotDir + "\\" + this.getClass().getName() + sm.getSearchText() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(driver.findElements(By.xpath(sm.getElementXpath())).size(), 1);
    }
}
