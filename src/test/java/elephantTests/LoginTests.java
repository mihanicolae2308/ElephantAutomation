package elephantTests;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import models.LoginModel;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.BaseTest;

import java.io.File;
import java.io.IOException;
import java.sql.*;

/**
 * Class for login tests including positive and negative scenarios.
 * Also a SQL data provider is included
 * @author Mihai Nicolae, 14-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class LoginTests extends BaseTest {
    @Parameters({"username", "password", "database"})
    @Test
    /**
     * Method for executing login positive scenario using credentials from the database.
     * Database connection credentials are taken from the configuration file (pom.xml).
     * @param   username    username for database connection
     * @param   password    password used for database connection
     * @param   database    name of the database where test data can be found
     */
    public void loginTest(String username, String password, String database) {

        test = extentReport.startTest("Login test");

        //Variable for monitoring the login state
        boolean successfulLogin = false;

        System.out.println("Login with user " + username + ", password " + password + " on DB " + database);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + database + "?useSSL=false&serverTimezone=UTC", username, password);
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from logindata;");

            //For each entry in the result set (row in the database table login method is called using email and password retrieved from the database
            if (res.next()) {
                driver.navigate().to(homePage + "autentificare");

                File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
                try {
                    FileUtils.copyFile(scrFile, new File(screenshotDir + "\\" + this.getClass().getName() + ".png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

                lp.login(res.getString("email"), res.getString("password"));

                File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
                try {
                    FileUtils.copyFile(scrFile2, new File(screenshotDir + "\\" + this.getClass().getName() + "_after_login.png"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String currentURL = driver.getCurrentUrl();

                System.out.println(currentURL);

                //If the URL after calling login method is not the login URL successfulLogin variable is set to true
                if (!currentURL.equals(homePage + "autentificare")) {
                    successfulLogin = true;
                    test.log(LogStatus.PASS, "No longer on login page.");
                }

                Assert.assertTrue(successfulLogin);

                //Loging out after a successful login and verifying that logout works
                if (successfulLogin) {
                    driver.navigate().to(homePage + "homepage");
                    WebElement myAccount = driver.findElement(By.xpath("//DIV[@class='hidden-xs header-account-display']"));
                    Actions actions = new Actions(driver);
                    actions.click(myAccount).perform();

                    //Verify that the logout button exists (so the user is logged in)
                    System.out.println("Verify logout button exists");
                    Assert.assertEquals(driver.findElements(By.xpath("//A[@id='logout-button']")).size(), 1);
                    test.log(LogStatus.PASS, "Logout button is present.");


                    System.out.println("Verifying that logout button is displayed");
                    Assert.assertTrue(driver.findElement(By.xpath("//A[@id='logout-button']")).isDisplayed());
                    test.log(LogStatus.PASS, "Logout button is displayed when being logged in");

                    WebElement logoutBtn = driver.findElement(By.xpath("//A[@id='logout-button']"));
                    System.out.println("Pressing logout!");
                    Actions act = new Actions(driver);
                    act.click(logoutBtn).perform();

                    mySleeper(2000);

                    Actions afterLogout = new Actions(driver);
                    WebElement myAccount2 = driver.findElement(By.xpath("//DIV[@class='hidden-xs header-account-display']"));
                    afterLogout.click(myAccount2).perform();

                    Assert.assertEquals(driver.findElements(By.xpath("//A[@href='/autentificare'][text()='Intra in cont']")).size(), 1);
                    test.log(LogStatus.PASS, "Authentication button exists.");
                    System.out.println("Verifying that autentication button is displayed");
                    Assert.assertTrue(driver.findElement(By.xpath("//A[@href='/autentificare'][text()='Intra in cont']")).isDisplayed());
                    test.log(LogStatus.PASS, "Authentication button is displayed after logging out.");
                }

            } else {
                Assert.fail("No login data available");
            }

            res.close();
            conn.close();
        } catch (SQLException sexc) {
            sexc.printStackTrace();
            Assert.fail("Could not connect to database.");
        }
    }

    @DataProvider(name = "SQLLoginDataProvider")
    /**
     * Data provider for login negative scenarios, using data email and password data from the database
     */
    public Object[][] sqlDataProvider() {
        int numberOfEntries = 0;
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + database + "?useSSL=false&serverTimezone=UTC", username, password);
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from loginnegativedata;");

            while (res.next()) {
                numberOfEntries++;
            }
            res.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        Object[][] dp = new Object[numberOfEntries][1];

        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + "testdata" + "?useSSL=false&serverTimezone=UTC", "testuser", "test");
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from loginnegativedata;");

            int i = 0;
            while (res.next()) {
                LoginModel model = new LoginModel();
                model.setEmail(res.getString("email"));
                model.setPassword(res.getString("password"));
                model.setExpectedEmailError(res.getString("emailerror"));
                model.setExpectedPasswordError(res.getString("passworderror"));
                dp[i][0] = model;
                i++;
            }
            res.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp;
    }


    @Test(dataProvider = "SQLLoginDataProvider")
    /**
     * Method for running login negative tests using test data from a SQL data provider.
     * The test scenario verifies that the correct error messages are displayed in case of
     * empty or incorrect values entered in login fields.
     * @param   lm  a model for login page
     * @see     LoginModel
     */
    public void loginTestNegative(LoginModel lm) {
        test = extentReport.startTest("Login Negative Test");
        driver.navigate().to(homePage + "autentificare");

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File(screenshotDir + "\\" + this.getClass().getName() + "negative_test.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

        lp.login(fixNullString(lm.getEmail()), fixNullString(lm.getPassword()));

        File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile2, new File(screenshotDir + "\\" + this.getClass().getName() + "negative_test_error.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        test.log(LogStatus.PASS, "Using " + "'" + fixNullString(lm.getEmail()) + "'" +
                " email and " + "'" + fixNullString(lm.getPassword()) + "'" + " password.");

        lp.checkPageErrors(fixNullString(lm.getEmail()), fixNullString(lm.getPassword()),
                fixNullString(lm.getExpectedEmailError()), fixNullString(lm.getExpectedPasswordError()));
    }

}