package pages;

import org.codehaus.plexus.util.StringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

/**
 * Class that defines a login page object.
 * It defines the web elements in the page and login and register methods.
 * @author  Mihai Nicolae, 14-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class LoginPage {
    @FindBy(how = How.ID, using = "login_username")
    private WebElement emailLoginField;

    @FindBy(how = How.ID, using = "login_password")
    private WebElement passwordLoginField;

    @FindBy(how = How.ID, using = "login_classic")
    private WebElement submitValidateLoginField;

    @FindBy(how = How.ID, using = "login_username-error")
    private WebElement loginEmailError;

    @FindBy(how = How.ID, using = "login_password-error")
    private WebElement loginPasswordError;

    /**
     * Method for performing login action.
     * After introducing email and password in the corresponding fields, the login button is clicked.
     * @param loginEmail    email string to be sent to email field.
     * @param loginPassword password string to be sent to password field.
     */
    public void login(String loginEmail, String loginPassword) {
        emailLoginField.clear();
        emailLoginField.sendKeys(loginEmail);
        passwordLoginField.clear();
        passwordLoginField.sendKeys(loginPassword);
        submitValidateLoginField.click();
    }

    public void checkPageErrors(String email, String password, String expectedEmailError, String expectedPasswordError) {
        if (email.equals("") || email.startsWith("@") || email.endsWith("@") || (StringUtils.countMatches(email, "@") != 1)) {
            Assert.assertEquals(loginEmailError.getText(), expectedEmailError);
        }

        if (password.equals("")) {
            Assert.assertEquals(loginPasswordError.getText(), expectedPasswordError);
        }
    }


}
