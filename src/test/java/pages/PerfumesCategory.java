package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class PerfumesCategory extends ProductCategories {
    @FindBy(how = How.XPATH, using = "//A[@href='/list/cosmetice-si-parfumuri/parfumuri?filtersex=Femei~~~Unisex'][text()='Parfumuri pentru femei']")
    public WebElement womenPerfumes;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/cosmetice-si-parfumuri/parfumuri?filtersex=Barbati~~~Unisex'][text()='Parfumuri pentru barbati']")
    public WebElement menPerfumes;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/cosmetice-si-parfumuri/cosmetice?filtersex=Femei'][text()='Cosmetice femei']")
    public WebElement womenCosmetics;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/cosmetice-si-parfumuri/cosmetice/cosmetice-barbati#__sZC5sZW9udGVAZWxlZmFudC5ybw'][text()='Cosmetice barbati']")
    public WebElement menCosmetics;

    public void goToSubcategory(WebElement we) {
        we.click();
    }
}
