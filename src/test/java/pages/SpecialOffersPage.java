package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 09-Dec-17.
 */
public class SpecialOffersPage {
    @FindBy(how = How.XPATH, using = "//DIV[@class='special-offers-title'][text()='Oferte Speciale']")
    public WebElement specialOffers;

    @FindBy(how = How.XPATH, using = "(//DIV[@class='special-offers-instructions-box'])[1]")
    public WebElement instructionsStep1;

    @FindBy(how = How.XPATH, using = "(//DIV[@class='special-offers-instructions-box'])[2]")
    public WebElement instructionsStep2;

    @FindBy(how = How.XPATH, using = "(//DIV[@class='special-offers-instructions-box'])[3]")
    public WebElement instructionsStep3;

    public void checkElementDisplayed(WebElement we) {
        Assert.assertTrue(we.isDisplayed());
    }
}
