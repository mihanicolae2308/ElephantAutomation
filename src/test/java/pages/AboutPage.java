package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 27-Nov-17.
 */
public class AboutPage {
    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Cod Unic de Inregistrare: RO 26396066'])[1]")
    public WebElement cui;

    @FindBy(how = How.XPATH, using = "(//SPAN)[21]")
    public WebElement email;

    @FindBy(how = How.XPATH, using = "//STRONG[text()='Scurt istoric']")
    public WebElement history;

    @FindBy(how = How.XPATH, using = "(//SPAN[contains(text(),'Tel.:')])[1]")
    public WebElement telephoneNumber;

    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Fax: 031.620.74.19'])[1]")
    public WebElement faxNumber;

    public void checkElementDisplayed(WebElement we) {
        Assert.assertTrue(we.isDisplayed());
    }
}
