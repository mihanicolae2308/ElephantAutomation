package pages;

import org.codehaus.plexus.util.StringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 10-Dec-17.
 */
public class RegisterPage {
    @FindBy(how = How.ID, using = "r_first_name")
    private WebElement firstNameField;

    @FindBy(how = How.ID, using = "r_last_name")
    private WebElement lastNameField;

    @FindBy(how = How.ID, using = "r_email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "r_password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "r_c_password")
    private WebElement passwordConfirmationField;

    @FindBy(how = How.ID, using = "sex_dl")
    private WebElement male;

    @FindBy(how = How.ID, using = "sex_dna")
    private WebElement female;

    @FindBy(how = How.ID, using = "terms_ok")
    private WebElement termsOK;

////////////////////////////////////////////////////
    @FindBy(how = How.ID, using = "r_sex-error")
    private WebElement sexError;

    @FindBy(how = How.ID, using = "r_first_name-error")
    private WebElement firstNameError;

    @FindBy(how = How.ID, using = "r_last_name-error")
    private WebElement lastNameError;

    @FindBy(how = How.ID, using = "r_email-error")
    private WebElement emailError;

    @FindBy(how = How.ID, using = "r_password-error")
    private WebElement passwordError;

    @FindBy(how = How.ID, using = "r_c_password-error")
    private WebElement passwordConfirmationError;

    @FindBy(how = How.ID, using = "terms_ok-error")
    private WebElement termsNotAcceptedError;

    @FindBy(how = How.ID, using = "register_classic")
    private WebElement submitButton;

    public void register(String firstName, String lastName, String email, String password, String passwordConfirmation,
                         String gender, String conditionsAccepted) {
        if (gender.equals("male")) {
            male.click();
        }
        if (gender.equals("female")) {
            female.click();
        }
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
        emailField.clear();
        emailField.sendKeys(email);
        passwordField.clear();
        passwordField.sendKeys(password);
        passwordConfirmationField.clear();
        passwordConfirmationField.sendKeys(passwordConfirmation);
        if (conditionsAccepted.equals("yes")) {
            termsOK.click();
        }

        submitButton.click();
    }

    public void checkPageErrors(String firstName, String lastName, String email, String password, String passwordConfirmation,
                                String gender, String conditionsAccepted, String expectedSexError, String expectedFirstNameError,
                                String expectedLastNameError, String expectedEmailError, String expectedPasswordError, String expectedPasswordConfirmationError,
                                String expectedTermsAcceptanceError) {

        if (!gender.equals("male") && !gender.equals("female")) {
            Assert.assertEquals(sexError.getText(), expectedSexError);
        }

        if (firstName.equals("")) {
            Assert.assertEquals(firstNameError.getText(), expectedFirstNameError);
        }

        if (lastName.equals("")) {
            Assert.assertEquals(lastNameError.getText(), expectedLastNameError);
        }

        if (email.equals("") || email.startsWith("@") || email.endsWith("@") || (StringUtils.countMatches(email, "@") != 1)) {
            Assert.assertEquals(emailError.getText(), expectedEmailError);
        }

        if (password.equals("") || password.length() < 6) {
            Assert.assertEquals(passwordError.getText(), expectedPasswordError);
        }

        if (passwordConfirmation.equals("") || !passwordConfirmation.equals(password)) {
            Assert.assertEquals(passwordConfirmationError.getText(), expectedPasswordConfirmationError);
        }

        if (!conditionsAccepted.equals("yes")) {
            Assert.assertEquals(termsNotAcceptedError.getText(), expectedTermsAcceptanceError);
        }
    }

}
