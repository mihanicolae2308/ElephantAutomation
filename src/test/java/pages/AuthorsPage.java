package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 09-Dec-17.
 */
public class AuthorsPage {
    @FindBy(how = How.XPATH, using = "(//SPAN[@class='dropt'])[36]")
    public WebElement aDXenopol;

    @FindBy(how = How.XPATH, using = "(//SPAN[@class='dropt'])[39]")
    public WebElement aDan;

    @FindBy(how = How.XPATH, using = "(//SPAN[@class='dropt'])[44]")
    public WebElement aDumitru;

    @FindBy(how = How.XPATH, using = "//A[@class=' litera_meniu link_litera'][text()='\n" +
            "\t\t\t\t\t\t\t\t\tM\n" +
            "\t\t\t\t\t\t\t\t']")
    public WebElement letterM;

    @FindBy(how = How.XPATH, using = "//A[@class=' litera_meniu link_litera'][text()='\n" +
            "\t\t\t\t\t\t\t\t\tP\n" +
            "\t\t\t\t\t\t\t\t']")
    public WebElement letterP;

    public void checkElementDisplayed(WebElement we) {
        Assert.assertTrue(we.isDisplayed());
    }
}
