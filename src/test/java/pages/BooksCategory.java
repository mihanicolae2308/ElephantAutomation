package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class BooksCategory extends ProductCategories {
    @FindBy(how = How.XPATH, using = "//A[@href='/carti'][text()='Carti']")
    public WebElement books;

    @FindBy(how = How.XPATH, using = "//A[@href='/edituri'][text()='Toate editurile']")
    public WebElement allPublishers;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/carti/carte-straina'][text()='Carte straina']")
    public WebElement foreignBooks;

    @FindBy(how = How.XPATH, using = "//A[@href='/ebooks'][text()='Toate eBook-urile']")
    public WebElement eBooks;

    public void goToSubcategory(WebElement we) {
        we.click();
    }
}
