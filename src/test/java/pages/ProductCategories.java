package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class ProductCategories {

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Parfumuri')]")
    public WebElement perfumes;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Carti']")
    public WebElement books;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Ceasuri']")
    public WebElement watches;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Jucarii, copii')]")
    public WebElement toys;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Electro']")
    public WebElement electronics;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Casa')]")
    public WebElement homeAndPetshop;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][contains(text(),'Fashion')]")
    public WebElement fashion;

    @FindBy(how = How.XPATH, using = "//A[@data-toggle='dropdown'][text()='Sport']")
    public WebElement sport;

    public void goToCategory(WebElement we) {
        we.click();
    }
}
