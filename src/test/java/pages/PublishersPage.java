package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 09-Dec-17.
 */
public class PublishersPage {
    @FindBy(how = How.XPATH, using = "//A[@href='/edituri/corint-11.html']")
    public WebElement corint;

    @FindBy(how = How.XPATH, using = "//A[@href='/edituri/herald-658.html']")
    public WebElement herald;

    @FindBy(how = How.XPATH, using = "//A[@href='/edituri/vellant-476.html']")
    public WebElement vellant;

    @FindBy(how = How.XPATH, using = "//A[@href='/edituri/epica-2586.html']")
    public WebElement epica;

    public void checkElementDisplayed(WebElement we) {
        Assert.assertTrue(we.isDisplayed());
    }
}
