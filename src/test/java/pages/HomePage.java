package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class HomePage {
    @FindBy(how = How.ID, using = "query")
    public WebElement query;

    @FindBy(how = How.ID, using = "searchsubmit")
    public WebElement searchButton;


    public void search(String text) {
        query.clear();
        query.sendKeys(text);
        searchButton.click();
    }
}
