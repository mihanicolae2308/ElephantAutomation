package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by Mihai on 09-Dec-17.
 */
public class ContactPage {
    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Cod Unic de Inregistrare: '])[1]")
    public WebElement cui;

    @FindBy(how = How.XPATH, using = "(//SPAN[text()='Email:'])[1]")
    public WebElement email;

    @FindBy(how = How.XPATH, using = "//A[@href='https://www.facebook.com/elefant.ro'][text()='Facebook.com/elefant.ro']")
    public WebElement facebook;

    @FindBy(how = How.XPATH, using = "(//SPAN[text()='031.9301'])[1]")
    public WebElement telephoneNumber;

    @FindBy(how = How.XPATH, using = "(//SPAN[text()='031.9301'])[1]")
    public WebElement faxNumber;

    public void checkElementDisplayed(WebElement we) {
        Assert.assertTrue(we.isDisplayed());
    }
}
