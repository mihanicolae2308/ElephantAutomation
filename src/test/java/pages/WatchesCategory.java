package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Mihai on 13-Dec-17.
 */
public class WatchesCategory extends ProductCategories {
    @FindBy(how = How.XPATH, using = "//A[@href='/list/ceasuri?filtersex=Femei~~~Unisex'][text()='Ceasuri de dama']")
    public WebElement womenWatches;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/ceasuri?filtersex=Barbati~~~Unisex'][text()='Ceasuri barbatesti']")
    public WebElement menWatches;

    @FindBy(how = How.XPATH, using = "//A[@href='/list/ceasuri?filtersex=Fetite~~~Baieti~~~Junior+Unisex'][text()='Ceasuri pentru copii']")
    public WebElement childrenWatches;

    public void goToSubcategory(WebElement we) {
        we.click();
    }
}
