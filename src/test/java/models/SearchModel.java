package models;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class SearchModel {
    private String searchText;
    private String elementXpath;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getElementXpath() {
        return elementXpath;
    }

    public void setElementXpath(String elementXpath) {
        this.elementXpath = elementXpath;
    }
}
