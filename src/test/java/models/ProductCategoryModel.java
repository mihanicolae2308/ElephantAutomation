package models;

/**
 * Created by Mihai on 12-Dec-17.
 */
public class ProductCategoryModel {
    private String webElementName;
    private String expectedUrl;

    public String getWebElementName() {
        return webElementName;
    }

    public void setWebElementName(String webElementName) {
        this.webElementName = webElementName;
    }

    public String getExpectedUrl() {
        return expectedUrl;
    }

    public void setExpectedUrl(String expectedUrl) {
        this.expectedUrl = expectedUrl;
    }
}
