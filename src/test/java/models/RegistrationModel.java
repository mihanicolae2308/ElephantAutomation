package models;

/**
 * Created by Mihai on 11-Dec-17.
 */
public class RegistrationModel {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String passwordConfirmation;
    private String gender;
    private String conditionsAccepted;
    private String expectedSexError;
    private String expectedFirstNameError;
    private String expectedLastNameError;
    private String expectedEmailError;
    private String expectedPasswordError;
    private String expectedPasswordConfirmationError;
    private String expectedTermsAcceptanceError;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getExpectedSexError() {
        return expectedSexError;
    }

    public void setExpectedSexError(String expectedSexError) {
        this.expectedSexError = expectedSexError;
    }

    public String getExpectedFirstNameError() {
        return expectedFirstNameError;
    }

    public void setExpectedFirstNameError(String expectedFirstNameError) {
        this.expectedFirstNameError = expectedFirstNameError;
    }

    public String getExpectedLastNameError() {
        return expectedLastNameError;
    }

    public void setExpectedLastNameError(String expectedLastNameError) {
        this.expectedLastNameError = expectedLastNameError;
    }

    public String getExpectedEmailError() {
        return expectedEmailError;
    }

    public void setExpectedEmailError(String expectedEmailError) {
        this.expectedEmailError = expectedEmailError;
    }

    public String getExpectedPasswordError() {
        return expectedPasswordError;
    }

    public void setExpectedPasswordError(String expectedPasswordError) {
        this.expectedPasswordError = expectedPasswordError;
    }

    public String getExpectedPasswordConfirmationError() {
        return expectedPasswordConfirmationError;
    }

    public void setExpectedPasswordConfirmationError(String expectedPasswordConfirmationError) {
        this.expectedPasswordConfirmationError = expectedPasswordConfirmationError;
    }

    public String getExpectedTermsAcceptanceError() {
        return expectedTermsAcceptanceError;
    }

    public void setExpectedTermsAcceptanceError(String expectedTermsAcceptanceError) {
        this.expectedTermsAcceptanceError = expectedTermsAcceptanceError;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getConditionsAccepted() {
        return conditionsAccepted;
    }

    public void setConditionsAccepted(String conditionsAccepted) {
        this.conditionsAccepted = conditionsAccepted;
    }
}
