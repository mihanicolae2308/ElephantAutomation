package models;

/**
 * Class that defines a login model to be used in login data providers and login tests
 * @author  Mihai Nicolae, 25-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class LoginModel {
    private String email;
    private String password;
    private String expectedEmailError;
    private String expectedPasswordError;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExpectedEmailError() {
        return expectedEmailError;
    }

    public void setExpectedEmailError(String expectedEmailError) {
        this.expectedEmailError = expectedEmailError;
    }

    public String getExpectedPasswordError() {
        return expectedPasswordError;
    }

    public void setExpectedPasswordError(String expectedPasswordError) {
        this.expectedPasswordError = expectedPasswordError;
    }
}

